//
//  ViewController.swift
//  Template4
//
//  Created by TISSA Technology on 3/1/21.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var rotateLbl: UILabel!
    @IBOutlet weak var view1: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
//                let gradientLayer = CAGradientLayer() // 1
//        gradientLayer.frame = self.view.bounds // 2
//                gradientLayer.colors = [self.randomColor().cgColor, self.randomColor().cgColor] // 3
//              view1.layer.addSublayer(gradientLayer)
        
//        rotateLbl.center.x += 300
//        UIView.animate(withDuration: 2) {
//            self.rotateLbl.center.x -= 300
//

        self.createQuestions { () -> () in
            self.newQuestion()
        }
       
            
        }
    
    
    func createQuestions(handleComplete:(()->())){
        // do something
        
                rotateLbl.center.x += 300
                UIView.animate(withDuration: 2) {
                    self.rotateLbl.center.x -= 300
                }
        handleComplete() // call it when finished stuff what you want
    }
        
    func newQuestion(){
        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    
    func randomColor() -> UIColor{
            let red = CGFloat(drand48())
            let green = CGFloat(drand48())
            let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 0.5)
        }
    
   
       
           
}

