//
//  SideMenu.swift
//  Template4
//
//  Created by TISSA Technology on 3/1/21.
//

import UIKit
import Alamofire

class SideMenu: UIViewController  {
    @IBOutlet weak var sidetableShow: UITableView!
    @IBOutlet weak var restaurantLbl: UILabel!

    var menunames = NSArray()
    var menuicons = NSArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
           
            menuicons = ["log-in.png","menu.png","information.png","communicate.png"]
            
            menunames = ["Login","Menu","About Us","Contact Us"]
            
            
        }else{
            
        let defaults = UserDefaults.standard
        let userlog = defaults.object(forKey: "Userlog")as! String
        if userlog == "reguser" {
        
            menuicons = ["menu.png","resume.png","empty-cart.png","note.png","information.png","communicate.png","logout.png"]
            
            menunames = ["Menu","Profile","Cart","Order History","About Us","Contact Us","Logout"]
            
        }else{
            menuicons = ["menu.png","empty-cart.png","note.png","information.png","communicate.png","logout.png"]
            
            menunames = ["Menu","Cart","Order History","About Us","Contact Us","Logout"]
        
        }
        
    }
        
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        sidetableShow.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "cell")
        
//        let gradient = getGradientLayer(bounds: CGRect(x: 0, y: 0, width: restaurantLbl.frame.size.width, height: restaurantLbl.frame.size.height))
//        restaurantLbl.textColor = gradientColor(bounds: restaurantLbl.bounds, gradientLayer: gradient)
    }
    
    func getGradientLayer(bounds : CGRect) -> CAGradientLayer{
    let gradient = CAGradientLayer()
    gradient.frame = bounds
    //order of gradient colors
    gradient.colors = [UIColor.red.cgColor,UIColor.blue.cgColor, UIColor.green.cgColor]
    // start and end points
    gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
    gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
    return gradient
    }

    func gradientColor(bounds: CGRect, gradientLayer :CAGradientLayer) -> UIColor? {
    UIGraphicsBeginImageContext(gradientLayer.bounds.size)
      //create UIImage by rendering gradient layer.
    gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
      //get gradient UIcolor from gradient UIImage
    return UIColor(patternImage: image!)
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
    dismiss(animated: true, completion: nil)
        
    }
 
}

// MARK: - uiTableViewDatasource
extension SideMenu: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menunames.count


    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SideMenuCell

      //  cell.selectionStyle = .none
        
        cell.iconimage.image =  UIImage(named: menuicons[indexPath.row] as! String)

        cell.iconname.text =  menunames[indexPath.row] as? String
        return cell
    }
    
        @IBAction func addBtnCLicked(_ sender: UIButton) {
    
            let alert = UIAlertController(title: nil, message: "Added to cart!",         preferredStyle: UIAlertController.Style.alert)

    //               alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: { _ in
    //                   //Cancel Action//
    //               }))
            alert.addAction(UIAlertAction(title: "OK",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            
                                     
            }))
            self.present(alert, animated: true, completion: nil)
         alert.view.tintColor = UIColor.black
    
    
        }
    
   
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return  62
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            
            if indexPath.row == 0 {
                
                let home = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                self.navigationController?.pushViewController(home, animated: true)
                
            }else if indexPath.row == 1 {
                
                let profile = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
                self.navigationController?.pushViewController(profile, animated: true)
                
            }else if indexPath.row == 2 {
                let about = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsPage") as! AboutUsPage
                self.navigationController?.pushViewController(about, animated: true)
                
            }else{
                let contact = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsPage") as! ContactUsPage
                self.navigationController?.pushViewController(contact, animated: true)
                
            }
            
        }else{
        
        let defaults = UserDefaults.standard
        let userlog = defaults.object(forKey: "Userlog")as! String
        if userlog == "reguser" {
        
        if indexPath.row == 0 {
            
            let home = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
            self.navigationController?.pushViewController(home, animated: true)
            
        }else if indexPath.row == 1 {
            
            let profile = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePage") as! ProfilePage
            self.navigationController?.pushViewController(profile, animated: true)
            
        }else if indexPath.row == 2 {
            let cart = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
            self.navigationController?.pushViewController(cart, animated: true)
            
        }else if indexPath.row == 3 {
            let order = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryPage") as! OrderHistoryPage
            self.navigationController?.pushViewController(order, animated: true)
            
        }else if indexPath.row == 4 {
            let about = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsPage") as! AboutUsPage
            self.navigationController?.pushViewController(about, animated: true)
            
        }else if indexPath.row == 5 {
            let contact = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsPage") as! ContactUsPage
            self.navigationController?.pushViewController(contact, animated: true)
            
        }else{
            
            Afterlogouthomerefresh()
        }
   
        }else{
          
            if indexPath.row == 0 {
                
                let home = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
                self.navigationController?.pushViewController(home, animated: true)
                
            }else if indexPath.row == 1 {
                let cart = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
                self.navigationController?.pushViewController(cart, animated: true)
                
            }else if indexPath.row == 2 {
                let order = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryPage") as! OrderHistoryPage
                self.navigationController?.pushViewController(order, animated: true)
                
            }else if indexPath.row == 3 {
                let about = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsPage") as! AboutUsPage
                self.navigationController?.pushViewController(about, animated: true)
                
            }else if indexPath.row == 4 {
                let contact = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsPage") as! ContactUsPage
                self.navigationController?.pushViewController(contact, animated: true)
                
            }else{
                
                Afterlogouthomerefresh()
            }
          
        }
            
        }
    }
   
    func Afterlogouthomerefresh() {
       let alert = UIAlertController(title: nil, message: "Are you sure you want to logout?",         preferredStyle: UIAlertController.Style.alert)

       alert.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.default, handler: { _ in

               UserDefaults.standard.removeObject(forKey: "AvlbCartId")
               UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
               UserDefaults.standard.removeObject(forKey: "custToken")
               UserDefaults.standard.removeObject(forKey: "custId")
           UserDefaults.standard.removeObject(forKey: "Usertype")
           UserDefaults.standard.synchronize()

           ERProgressHud.sharedInstance.show(withTitle: "Loading...")
           self.logout()

       }))
       alert.addAction(UIAlertAction(title: "NO",
                                     style: UIAlertAction.Style.default,
                                     handler: {(_: UIAlertAction!) in
                                       //Sign out action


       }))

       if presentedViewController == nil {
           self.present(alert, animated: true, completion: nil)
       } else{
           self.dismiss(animated: false) { () -> Void in
               self.present(alert, animated: true, completion: nil)
             }
       }

    //   present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black


   }
    
}

// MARK: - Api
extension SideMenu {
    
    func logout(){
        
        let defaults = UserDefaults.standard
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerid = defaults.integer(forKey: "custId")
        let customeridStr = String(customerid)
        print("customeridStr = \(customeridStr)")
        print("customer token = \(String(describing: admintoken))")
        print("global customer token = \(GlobalClass.customertoken)")

        
        let autho = "token \(GlobalClass.customertoken)"
        
        let urlString = GlobalClass.DevlopmentApi+"rest-auth/logout/"
        
           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]
      

        AF.request(urlString, method: .post, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                 
                               //  let dict :NSDictionary = response.value! as! NSDictionary
                                 
                                    print(response)
                                  
                                    ERProgressHud.sharedInstance.hide()

                                    let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                    
                                    self.navigationController?.pushViewController(login, animated: true)
                                    
                                 
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()

                   // self.SessionAlert()
                          
                                    
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
}

// MARK: - AlertController
extension SideMenu {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
