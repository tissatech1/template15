//
//  collectionCell.swift
//  Template4
//
//  Created by TISSA Technology on 3/2/21.
//

import UIKit

class collectionCell: UICollectionViewCell {

    @IBOutlet weak var collectionbackView: UIView!
    @IBOutlet weak var collectionImageView: UIImageView!
    @IBOutlet weak var collectionnameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
