//
//  MenuListPage.swift
//  Template4
//
//  Created by TISSA Technology on 3/5/21.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage

class MenuListPage: UIViewController {
    @IBOutlet weak var dishtable: UITableView!
    @IBOutlet weak var itemview: UIView!
    @IBOutlet weak var cartcountLbl: UILabel!
    @IBOutlet weak var tabletopheight: NSLayoutConstraint!

    var dishlist = NSMutableArray()
    var passedcategoryid = Int()
    var passproductid = Int()
    var quantityNumber = Int()
    var PageCount = Int()
    var isLoading = false
    var restStatusStr = String()

    func loadData() {
        isLoading = true
      //  ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        GetDishList()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabletopheight.constant = 8
        dishlist = []
        PageCount = 1
        dishtable.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "cell")
        dishtable.rowHeight = UITableView.automaticDimension
        dishtable.estimatedRowHeight = 197
        
        itemview.layer.cornerRadius = 5
        itemview.layer.borderColor = UIColor.lightGray.cgColor
        itemview.layer.borderWidth = 1
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
       
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            adminlogincheck()
        }else{
            cartcountApi()
            GetDishList()
            gettiming()
        }
        
    }
   
    func randomColor() -> UIColor{
            let red = CGFloat(drand48())
            let green = CGFloat(drand48())
            let blue = CGFloat(drand48())
            return UIColor(red: red, green: green, blue: blue, alpha: 0.2)
        }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        let cart = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
        self.navigationController?.pushViewController(cart, animated: false)
            
    }
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
    }

    
    @IBAction func cartBtnClicked(_ sender: UIButton) {
    
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            
        }else{
            
            if restStatusStr == "open" {
    let cart = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
    self.navigationController?.pushViewController(cart, animated: true)
                
            }else{
                
            }
    }
    }
    
}

// MARK: - uiTableViewDatasource
extension MenuListPage: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastData = self.dishlist.count - 1
        if !isLoading && indexPath.row == lastData {
            PageCount += 1
            self.loadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dishlist.count
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuCell
        cell.selectionStyle = .none
 
        cell.transform = CGAffineTransform(scaleX: 0, y : 0)
               UIView.animate(withDuration: 0.3, animations: {
                   cell.transform = CGAffineTransform(scaleX: 1, y : 1)
               })
        
        let dictObj = self.dishlist[indexPath.row] as! NSDictionary
        cell.descriptionLbl.text = dictObj["extra"] as? String
        cell.dishnameLbl.text = dictObj["product_name"] as? String
       
        var urlStr = String()
        if dictObj["product_url"] is NSNull || dictObj["product_url"] == nil{

                    urlStr = ""

                }else{
                    urlStr = dictObj["product_url"] as! String
                }
              
                let url = URL(string: urlStr )


        cell.dishimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.dishimage.sd_setImage(with: url) { (image, error, cache, urls) in
                    if (error != nil) {
                        // Failed to load image
                        cell.dishimage.image = UIImage(named: "noimage.png")
                    } else {
                        // Successful in loading image
                        cell.dishimage.image = image
                    }
                }
        
        var conprice = String()
        let rupee = "$"
        if let pricedata = dictObj["price"] as? String {
             conprice = pricedata
        }else if let pricedata = dictObj["price"] as? NSNumber {
            
             conprice = pricedata.stringValue
        }
    
        cell.priceshowLbl.text = rupee + conprice
        
        cell.outerview.layer.cornerRadius = 8
        cell.outerview.layer.shadowColor = UIColor.lightGray.cgColor
        cell.outerview.layer.shadowOpacity = 1
        cell.outerview.layer.shadowOffset = .zero
        cell.outerview.layer.shadowRadius = 8
        cell.addtocartBtn.layer.cornerRadius = 8
        cell.addtocartBtn.layer.borderWidth = 1
        cell.addtocartBtn.layer.borderColor = UIColor.black.cgColor
        cell.dishimage.layer.masksToBounds = false
        cell.dishimage.layer.cornerRadius = 8
        cell.dishimage.clipsToBounds = true
        cell.outerview.layer.cornerRadius = 8
        cell.outerview.backgroundColor = self.randomColor()
        
       cell.addtocartBtn.tag = indexPath.row
       cell.addtocartBtn.addTarget(self, action: #selector(addBtnCLicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
        @IBAction func addBtnCLicked(_ sender: UIButton) {
    
             let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
               
                let alert = UIAlertController(title:nil, message: "Please login to continue. We can't let just anyone have this access to ordering!",         preferredStyle: UIAlertController.Style.alert)

                alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
                   
                }))
                alert.addAction(UIAlertAction(title: "SIGN IN",
                                              style: UIAlertAction.Style.default,
                                              handler: {(_: UIAlertAction!) in
                                                let home = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                                self.navigationController?.pushViewController(home, animated: true)
                                                
                }))
                
                self.present(alert, animated: true, completion: nil)
                alert.view.tintColor = UIColor.black
                
            }else{
                if restStatusStr == "open" {
            
            let dictObj = self.dishlist[sender.tag] as! NSDictionary
            
            passproductid = dictObj["product_id"] as! Int
            quantityNumber = 1
            
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")

            addToCart()
                    
                }else{
                    
                }
            }
    
        }
    
   
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return  UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if restStatusStr == "open" {
        
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "MenuDetailPage") as! MenuDetailPage

        let dictObj = self.dishlist[indexPath.row] as! NSDictionary

     detail.passproductid = dictObj["product_id"] as! Int
     detail.dishnameStr = (dictObj["product_name"] as? String)!
     detail.descriptionStr = (dictObj["extra"] as? String)!

     if dictObj["product_url"] is NSNull {
         detail.dishimageStr = ""
     }else{
         detail.dishimageStr = (dictObj["product_url"] as? String)!
     }
     detail.passunitprice = (dictObj["price"] as? String)!
     
     let restaurantid = dictObj["restaurant"]as! Int
        detail.passedcategoryidmenu = passedcategoryid
     let restid = String(restaurantid)
     let defaults = UserDefaults.standard
     
     defaults.set(restid, forKey: "clickedStoreId")
        
        self.navigationController?.pushViewController(detail, animated: true)
        }else{
            
            
        }
        
    }
    
}

extension MenuListPage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
    }
}

// MARK: - AlertController
extension MenuListPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
 

// MARK: - Api LIst
extension MenuListPage {
    
    //MARK: Admin Login
    
    func adminlogincheck(){

        let urlString = GlobalClass.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalClass.adminusername, "password":GlobalClass.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                            
                            self.gettiming()
                            self.GetDishList()
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
    //MARK: Webservice Call for add to cart

    func addToCart() {
       
        print(passproductid)
        
        let defaults = UserDefaults.standard
        
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        if defaults.object(forKey: "AvlbCartId") == nil {
          
            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
           
            
        }else{
        
         let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            let cartidStr = String(avlCartId)
            
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let admintoken = defaults.object(forKey: "custToken")as? String
            
            print("customer token -\(String(describing: admintoken))")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let urlString = GlobalClass.DevlopmentApi+"cart-item/"
        
            let now = Date()

                let formatter = DateFormatter()

                formatter.timeZone = TimeZone.current

                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS" ///2020-10-19 12:05:10.625

                let dateString = formatter.string(from: now)
            
            print(dateString)
            
            let commentStr = ""
            
           print("commentStr - \(commentStr)")
            
            let cartdatacount = defaults.object(forKey: "cartdatacount")as! String
            
            let metadataDict = ["updated_at":dateString,"extra":commentStr, "quantity":quantityNumber as Int, "cart_id":avlCartId as Any,"product_id":passproductid as Any,"ingredient_id":0,"sequence_id":cartdatacount]
          
            let productarr = [metadataDict]
            print("product element Dict - \(productarr)")
            
            var ingredientarray = Array<Any>()
            ingredientarray.insert(metadataDict, at: 0)
            print("Array product with ingredient added - \(ingredientarray)")

            
            let fileUrl = URL(string: urlString)

            var request = URLRequest(url: fileUrl!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(autho, forHTTPHeaderField: "Authorization")
            request.setValue(customeridStr, forHTTPHeaderField: "user_id")
            request.setValue(cartidStr, forHTTPHeaderField: "cart_id")
            request.setValue("cart-item", forHTTPHeaderField: "action")


            request.httpBody = try! JSONSerialization.data(withJSONObject: ingredientarray)

            AF.request(request)
                .responseJSON { response in
                    // do whatever you want here
                    switch response.result {
                                            case .success:
                                                print(response)
                    
                                                if response.response?.statusCode == 201{
                    
                                                    ERProgressHud.sharedInstance.hide()
                    
                                                 let alert = UIAlertController(title: nil, message: "Product added successfully into the cart",         preferredStyle: UIAlertController.Style.alert)
                    
                    
                                                   alert.addAction(UIAlertAction(title: "OK",
                                                                                 style: UIAlertAction.Style.default,
                                                                                 handler: {(_: UIAlertAction!) in
                    
                                                                                    self.cartcountApi()
                    
                                                   }))
                                                    
                                                    
                                                   self.present(alert, animated: true, completion: nil)
                                                    alert.view.tintColor = UIColor.black
                    
                                                }else{
                    
                                                    if response.response?.statusCode == 401{
                    
                                                        ERProgressHud.sharedInstance.hide()
                                                        self.SessionAlert()
                    
                                                    }else if response.response?.statusCode == 500{
                    
                                                        ERProgressHud.sharedInstance.hide()
                    
                                                        let dict :NSDictionary = response.value! as! NSDictionary
                    
                                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                    }else{
                    
                                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                       }
                    
                                                }
                    
                                                break
                                            case .failure(let error):
                                                ERProgressHud.sharedInstance.hide()
                    
                                                print(error.localizedDescription)
                    
                                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                    
                                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                    
                                                let msgrs = "URLSessionTask failed with error: The request timed out."
                    
                                                if error.localizedDescription == msg {
                    
                                            self.showSimpleAlert(messagess:"No internet connection")
                    
                                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
                                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                    
                                                }else{
                    
                                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                                }
                    
                                                   print(error)
                                            }
            }
            
            
        }

}
   
    func cartcountApi()
   {

      let defaults = UserDefaults.standard
      let customerid = defaults.integer(forKey: "custId")

        //  let customerid = 16
        
    let urlString = GlobalClass.DevlopmentGraphql


 //   let stringempty = "query{cartItemCount(token:\"\(GlobalObjects.globGraphQlToken)\", customerId :\(customerid)){\n count\n }\n}"

        let restidd = GlobalClass.restaurantGlobalid
       
        
        let stringempty = "query{cartItemCount(token:\"\(GlobalClass.globGraphQlToken)\",customerId :\(customerid),restaurantId:\(restidd)){\n count\n }\n}"
        
            AF.request(urlString, method: .post, parameters: ["query": stringempty],encoding: JSONEncoding.default, headers: nil).responseJSON {
           response in
             switch response.result {
                           case .success:
                              // print(response)

                               if response.response?.statusCode == 200{

                              let dict :NSDictionary = response.value! as! NSDictionary
                             // print(dict)
                                
                                if dict.count == 2 {

                                   // self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    self.cartcountLbl.text = "0"
                                    
                                    let countStr = self.cartcountLbl.text
                                    let defaults = UserDefaults.standard
                                    defaults.set(countStr, forKey: "cartdatacount")
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    
                                }else{
                                
                                
                                
                                let status = dict.value(forKey: "data")as! NSDictionary
                              print(status)


                                let newdict = status.value(forKey: "cartItemCount")as! NSDictionary

                                let num = newdict["count"] as! Int

                                self.cartcountLbl.text = String(num)

                                    let countStr = self.cartcountLbl.text
                                    let defaults = UserDefaults.standard
                                    defaults.set(countStr, forKey: "cartdatacount")
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    
                                }
                              //  self.dissmiss()

                               }else{

                                
                                if response.response?.statusCode == 500{
                                    
                              //      ERProgressHud.sharedInstance.hide()

                              //      let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                 //   self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else if response.response?.statusCode == 401{
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                


                               }

                               break
                           case .failure(let error):
                            
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow internet detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
           }


        }
    
    //MARK: Webservice Call for Offers
        
        func GetDishList(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            
            let autho = "token \(admintoken)"
            
            var urlString = String()

            urlString = GlobalClass.DevlopmentApi+"catalog/?restaurant_id=\(GlobalClass.restaurantGlobalid)&category_id=\(passedcategoryid)&status=ACTIVE&page=\(PageCount)"
                
            
            print(" category clicked menu api - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                   // print(response)

                                    if response.response?.statusCode == 200{
                                       
                                     let dict :NSDictionary = response.value! as! NSDictionary
                                     
                                        let list:NSArray = dict.value(forKey: "results") as! NSArray

                                        
                                        print(dict)
                                        
                                        
                                        if list.count == 0 {
                                           
                                            self.dishlist = []
                                            self.dishtable.reloadData()

                                            ERProgressHud.sharedInstance.hide()
                                         
                                        self.showSimpleAlert(messagess: "No menu available in this category")
                                            
                                        }else{
                                            
                                            self.dishlist .addObjects(from: list as! [Any]);                                            self.dishtable.reloadData()
                                            
                                            ERProgressHud.sharedInstance.hide()

                                                }

                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {
                                        
                                self.showSimpleAlert(messagess:"No internet connection")
                                        
                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                        
                                self.showSimpleAlert(messagess:"Slow internet detected")
                                        
                                    }else{
                                    
                                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
                }
                
            }
    
    //MARK: Webservice Call timing
        
        
        func gettiming(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            
            let autho = "token \(admintoken)"
            
            let urlString = GlobalClass.DevlopmentApi+"hour/?restaurant_id=\(GlobalClass.restaurantGlobalid)"
            
               
            print(" categoryurl - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                        
                                        let dict :NSDictionary = response.value! as! NSDictionary
                                       
                                        let data  = dict["status"]as! String
                                        
                                        self.restStatusStr = data
                                        
                                       // self.restStatusStr = "open"
                                        
                                       // data = "closed"
                                        
                                        if data == "closed" {
                                         
                                            print("restaurant is - \(data)")
                                            
                                            self.tabletopheight.constant = 38
//
//                                            self.menulistView.alpha = 0.5
//
//                                            self.categoryview.alpha = 0.5
                                            
                                        }else{
                                            
                                            self.tabletopheight.constant = 8
                                            
                                            print("restaurant is - open")
                                        }
                                        
                                       // self.GetMenuList()
                                        
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {

                                self.showSimpleAlert(messagess:"No internet connection")

                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                                self.showSimpleAlert(messagess:"Slow Internet Detected")

                                    }else{
                                    
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                    
                                    
                                    
                                }
                }
                
          
                
            }
    
    
    }

