//
//  ContactUsPage.swift
//  Template4
//
//  Created by TISSA Technology on 3/10/21.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage
class ContactUsPage: UIViewController {
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!

    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image4: UIImageView!

    @IBOutlet weak var phTF: UILabel!
    @IBOutlet weak var emailTF: UILabel!
    @IBOutlet weak var addressTF: UILabel!
    @IBOutlet weak var timeTF: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view1.layer.cornerRadius = 20
        view1.layer.borderWidth = 1
        view1.layer.borderColor = randomColor().cgColor
        
        view2.layer.cornerRadius = 20
        view2.layer.borderWidth = 1
        view2.layer.borderColor = randomColor().cgColor
        
        view3.layer.cornerRadius = 20
        view3.layer.borderWidth = 1
        view3.layer.borderColor = randomColor().cgColor
        
        view4.layer.cornerRadius = 20
        view4.layer.borderWidth = 1
        view4.layer.borderColor = randomColor().cgColor

        let oldCenter1 = image1.center
        let newCenter1 = CGPoint(x: oldCenter1.x - 100, y: oldCenter1.y)

        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear, animations: {
            self.image1.center = newCenter1
        }) { (success: Bool) in
          print("Done moving image")
          }
        
        let oldCenter2 = image2.center
        let newCenter2 = CGPoint(x: oldCenter2.x - 100, y: oldCenter2.y)

        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear, animations: {
            self.image2.center = newCenter2
        }) { (success: Bool) in
          print("Done moving image")
          }
        
        let oldCenter3 = image3.center
        let newCenter3 = CGPoint(x: oldCenter3.x - 100, y: oldCenter3.y)

        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear, animations: {
            self.image3.center = newCenter3
        }) { (success: Bool) in
          print("Done moving image")
          }
        
        let oldCenter4 = image4.center
        let newCenter4 = CGPoint(x: oldCenter4.x - 100, y: oldCenter4.y)

        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear, animations: {
            self.image4.center = newCenter4
        }) { (success: Bool) in
          print("Done moving image")
          }
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        getrestaurantApi()
        
    }
    
    func randomColor() -> UIColor{
            let red = CGFloat(drand48())
            let green = CGFloat(drand48())
            let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 0.5)
    }
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
    }
   
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    

}

extension ContactUsPage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
    }
}

// MARK: - Api
extension ContactUsPage {
    
    func getrestaurantApi(){
        
        var admintoken = String()
        let defaults = UserDefaults.standard
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
       
            admintoken = (defaults.object(forKey: "adminToken")as? String)!
            
        }else{
            
            admintoken = (defaults.object(forKey: "custToken")as? String)!
            
        }
        
        let autho = "token \(admintoken)"
        
        let urlString = GlobalClass.DevlopmentApi+"restaurant/\(GlobalClass.restaurantGlobalid)/"

           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]
      

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                   
                                   let dataPH  = dict["phone"]as? String
                                    let dataEmail  = dict["email"]as? String
                                    let dataADD  = dict["address"]as! String
                                    let dataCITY  = dict["city"]as! String
                                    let dataState  = dict["state"]as! String
                                    let dataZip  = dict["zip"]as! String
                                    let dataTime  = dict["working_hours"]as? String

                                    self.phTF.text! = dataPH ?? "(210)5248161"
                                    self.emailTF.text! = dataEmail ?? "maduraimes@gmail.com"
                                    self.addressTF.text! = "\(dataADD) \(dataCITY), \(dataState) \(dataZip)"
                                    
                                    self.timeTF.text! = dataTime ?? "Monday - Friday : 9 am to 5 pm Saturday and Sunday : Closed"
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()

                    self.SessionAlert()
                          
                                    
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
}

// MARK: - AlertController
extension ContactUsPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
