//
//  SideMenuCell.swift
//  Template4
//
//  Created by TISSA Technology on 3/5/21.
//

import UIKit

class SideMenuCell: UITableViewCell {
    @IBOutlet weak var iconimage: UIImageView!
    @IBOutlet weak var iconname: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
