//
//  OrderHistoryPage.swift
//  Template4
//
//  Created by TISSA Technology on 3/10/21.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage

class OrderHistoryPage: UIViewController {
    @IBOutlet weak var orderCollection: UICollectionView!
    var orderlistaraay = NSArray()
    var orderclickid = String()
    
    lazy var cellSizes: [CGSize] = {
        var cellSizes = [CGSize]()
        
        for _ in 0...100 {
            let random = Int(arc4random_uniform((UInt32(100))))
            
           // cellSizes.append(CGSize(width: 140, height: 122 + random))
            cellSizes.append(CGSize(width: 175, height: 236))
        }
        
        return cellSizes
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        getorderlist()
        
        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.minimumColumnSpacing = 10
        layout.minimumInteritemSpacing = 10
        
        orderCollection.collectionViewLayout = layout
        
        let viewNib = UINib(nibName: "OrderCell", bundle: nil)
        orderCollection.register(viewNib, forCellWithReuseIdentifier: "cell")

    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
    }
  
}

// MARK: - UICollectionViewDataSource
extension OrderHistoryPage: UICollectionViewDataSource,UICollectionViewDelegate {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return orderlistaraay.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OrderCell
       
        cell.outerview.layer.cornerRadius = 8
        cell.outerview.layer.shadowColor = UIColor.lightGray.cgColor
        cell.outerview.layer.shadowOpacity = 1
        cell.outerview.layer.shadowOffset = .zero
        cell.ordenumberView.layer.cornerRadius = cell.ordenumberView.frame.width / 2;
        cell.ordenumberView.layer.masksToBounds = true
        cell.ordenumberView.layer.shadowColor = UIColor.lightGray.cgColor
        cell.ordenumberView.layer.shadowOpacity = 1
        cell.ordenumberView.layer.shadowOffset = .zero
        cell.viewdetailsLbl.textColor = self.randomColor()
        
        cell.deleteBtnSh.tag = indexPath.row
        cell.deleteBtnSh.addTarget(self, action: #selector(DeleteBtnPredded(_:)), for: .touchUpInside)
        
        let dictObj = self.orderlistaraay[indexPath.row] as! NSDictionary

        let urlStr = dictObj["status"] as! String
    
         if urlStr == "CONFIRMED" {
             
             cell.orderStatusLbl.text = "Confirmed"
            cell.deleteBtnSh.isHidden = true
             
         }else if urlStr == "SUCCESS"{
             
             cell.orderStatusLbl.text = "Success"
            cell.deleteBtnSh.isHidden = true
             
         }else if urlStr == "FAIL"{
             
             cell.orderStatusLbl.text = "Failed"
            cell.deleteBtnSh.isHidden = true
             
         }else if urlStr == "READY_TO_FULFILL"{
             
             cell.orderStatusLbl.text = "Ready to fulfill"
             cell.deleteBtnSh.isHidden = true
             
         }else if urlStr == "READY_TO_PICK"{
             
             cell.orderStatusLbl.text = "Ready to pick"
             cell.deleteBtnSh.isHidden = true
             
         }else if urlStr == "PICKED"{
             
             cell.orderStatusLbl.text = "Picked"
             cell.deleteBtnSh.isHidden = true
             
         }else if urlStr == "COMPLETE"{
             
             cell.orderStatusLbl.text = "Completed"
             cell.deleteBtnSh.isHidden = true
             
         }else if urlStr == "CANCELED"{
             
             cell.orderStatusLbl.text = "Cancelled"
             cell.deleteBtnSh.isHidden = true
             
         }
        
        let rupee = "$"
         
        let priceLbl = dictObj["amount"]as! String

        cell.ordertotalLbl.text =  rupee + priceLbl
        
        let strdate = (dictObj["created_at"] as! String)

        let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
                dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                let date = dateFormatter.date(from: strdate)// create   date from string

                // change to a readable time format and change to local time zone
       if date == nil {
        
           let dateFormatter = DateFormatter()
                   dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                   dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                   let date1 = dateFormatter.date(from: strdate)
           dateFormatter.dateFormat = "MMMM dd,yyyy"
           dateFormatter.timeZone = NSTimeZone.local
           let timeStamp1 = dateFormatter.string(from: date1!)
   
  
   cell.orderDateLbl.text = timeStamp1
           
       }else{
       
                dateFormatter.dateFormat = "MMMM dd,yyyy"
                dateFormatter.timeZone = NSTimeZone.local
                let timeStamp = dateFormatter.string(from: date!)
       
        cell.orderDateLbl.text = timeStamp
       }
        
        let storedata:NSDictionary = dictObj["order"]as! NSDictionary
        
        let orderno = storedata["order_id"] as! Int
       
       cell.orderNoLbl.text = String(orderno)
        
       
        return cell
    }
    
    @IBAction func DeleteBtnPredded(_ sender: UIButton) {

        let index = sender.tag
       
        let dictObj = self.orderlistaraay[index] as! NSDictionary
        
        let storedata:NSDictionary = dictObj["order"]as! NSDictionary
        
        let orderno = storedata["order_id"] as! Int
        
        orderclickid = String(orderno)
        
       
        let alert = UIAlertController(title: nil, message: "Are you sure you want to cancel?",         preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action//
        }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
                                        ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                         self.deleteorderapi()
                                         
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
        
        
        
    }
    
    func randomColor() -> UIColor{
            let red = CGFloat(drand48())
            let green = CGFloat(drand48())
            let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
        }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let orders = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsPage") as! OrderDetailsPage
        
        let dictObj = self.orderlistaraay[indexPath.row] as! NSDictionary
        
        let storedata:NSDictionary = dictObj["order"]as! NSDictionary
        
        let storenamedetail:NSDictionary = dictObj["restaurant"]as! NSDictionary
        orders.storenamepassStr = storenamedetail["name"]as! String
        
        orders.orderIDGet = storedata["order_id"] as! Int
        
        if dictObj["shippingmethod"] is NSNull {
            orders.shippingmethodStrpass = "Restaurant Pickup"
        }else{
        
        let shippingMDict:NSDictionary = dictObj["shippingmethod"]as! NSDictionary
        
            orders.shippingmethodStrpass = shippingMDict["name"]as! String
            
        }
        orders.paymentmethodStrpass = dictObj["payment_method"]as! String
        
        let defaults = UserDefaults.standard
        defaults.set(storedata, forKey: "passOrderResultfromlist")
        
        
        self.navigationController?.pushViewController(orders, animated: true)
    }
    
    
}

// MARK: - CollectionViewWaterfallLayoutDelegate
extension OrderHistoryPage: CollectionViewWaterfallLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return cellSizes[indexPath.item]
    }
}

extension OrderHistoryPage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
    }
}

// MARK: - Api LIst
extension OrderHistoryPage {
    
    func getorderlist()  {
        
        let defaults = UserDefaults.standard
        
      //  let admintoken = defaults.object(forKey: "adminToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        let admintoken = defaults.object(forKey: "custToken")as? String
           
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"customer-payment/?customer_id=\(customerId)&restaurant_id=\(GlobalClass.restaurantGlobalid)"
         
        
        print("oderitems get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "action": "customer-payment"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    self.orderlistaraay  = response.value! as! NSArray
                                     
                                    print( self.orderlistaraay)
                                    
                                    if self.orderlistaraay.count == 0 {
                                       
                                        ERProgressHud.sharedInstance.hide()
                                        self.showSimpleAlert(messagess:"No order available")
                                        
                                        orderCollection.reloadData()
                                        
                                    }else{
                                        
                                        
                                        orderCollection.reloadData()
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        
                                    }
                                   
                    
                                 
                                }else{
                                    
                        if response.response?.statusCode == 401{
                                    
                            ERProgressHud.sharedInstance.hide()
                        self.SessionAlert()
                          
                           }else if response.response?.statusCode == 500{
                                        
                            ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func deleteorderapi()  {
        
        let defaults = UserDefaults.standard
        
      //  let admintoken = defaults.object(forKey: "adminToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        let admintoken = defaults.object(forKey: "custToken")as? String
           
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"payment/\(orderclickid)"
        
       

            
        print("delete get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "order_id": orderclickid,
                "user_id": customeridStr,
                "action": "payment"
            ]

        AF.request(urlString, method: .delete, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                
                                   
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title: "Order cencel successfully", message: nil,         preferredStyle: UIAlertController.Style.alert)

                                  
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                    //Sign out action
                                                                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                                                                getorderlist()
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                   
                    
                                 
                                }else{
                                    
                        if response.response?.statusCode == 401{
                                    
                            ERProgressHud.sharedInstance.hide()
                        self.SessionAlert()
                          
                           }else if response.response?.statusCode == 500{
                                        
                            ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                            
                           }else if response.response?.statusCode == 400{
                            
                            ERProgressHud.sharedInstance.hide()

                            let dict :NSDictionary = response.value! as! NSDictionary
                            
                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
               }else{
                                        
                      self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
}


// MARK: - AlertController
extension OrderHistoryPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
