//
//  MenuCell.swift
//  Template4
//
//  Created by TISSA Technology on 3/5/21.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var outerview: UIView!
    @IBOutlet weak var dishimage: UIImageView!
    @IBOutlet weak var dishnameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var addtocartBtn: UIButton!
    @IBOutlet weak var priceshowLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
