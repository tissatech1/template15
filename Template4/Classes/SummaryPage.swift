//
//  SummaryPage.swift
//  Template4
//
//  Created by TISSA Technology on 3/9/21.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage
class SummaryPage: UIViewController {
    @IBOutlet weak var cardView1: UIView!
    @IBOutlet weak var cardView2: UIView!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var sellerCollectionView: UICollectionView!
    
    @IBOutlet weak var orderNoLbl: UILabel!
    @IBOutlet weak var orderstorename: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var subtotalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var discountlbl: UILabel!
    @IBOutlet weak var orderTotalLbl: UILabel!
    @IBOutlet weak var addline1Lbl: UILabel!
    @IBOutlet weak var addline2Lbl: UILabel!
    @IBOutlet weak var addline3Lbl: UILabel!
    
    var  orderIDGet = Int()
    var fetchedItems = NSArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        cardView1.layer.cornerRadius = 10
        cardView1.layer.borderWidth = 2
        cardView1.layer.borderColor = self.randomColor().cgColor
        
        cardView2.layer.cornerRadius = 10
        cardView2.layer.borderWidth = 2
        cardView2.layer.borderColor = self.randomColor().cgColor

        homeBtn.layer.borderWidth = 2
        homeBtn.layer.borderColor = UIColor.darkGray.cgColor
        homeBtn.layer.cornerRadius = 20
        
        let viewNib2 = UINib(nibName: "summaryCell", bundle: nil)
        sellerCollectionView.register(viewNib2, forCellWithReuseIdentifier: "cell")
        
        setdata()
         
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
         orderitems()
        orderstorename.text = "Restaurant Name"
    }
    
    func randomColor() -> UIColor{
            let red = CGFloat(drand48())
            let green = CGFloat(drand48())
            let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 0.4)
    }

    func setdata() {
        let defaultns = UserDefaults.standard
        let order:NSDictionary = defaultns.object(forKey: "passOrderResult")as! NSDictionary

        print("orderInfo dict - \(order)")
        
        let numbersh = order["order_id"]as! Int
        
        self.orderNoLbl.text! = String(numbersh)

        let strdate = order["created_at"]as! String
        
      
        
        let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
                dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                let date = dateFormatter.date(from: strdate)// create   date from string

                // change to a readable time format and change to local time zone
                dateFormatter.dateFormat = "MMMM dd, yyyy"
                dateFormatter.timeZone = NSTimeZone.local
                let timeStamp = dateFormatter.string(from: date!)
        
        
        self.orderDateLbl.text! = timeStamp
        self.subtotalLbl.text! = "£\(order["subtotal"]as! String)"
      //  self.tiplbl.text! = "£\(order["tip"]as! String)"
      //  self.serviceFeeLbl.text! = "$\(order["service_fee"]as! String)"
        self.taxLbl.text! = "£\(order["tax"]as! String)"
      //  self.shippingFeeLbl.text! = "$\(order["shipping_fee"]as! String)"
        self.discountlbl.text! = "£\(order["discount"]as! String)"
        self.orderTotalLbl.text! = "£\(order["total"]as! String)"
        
       
        
                    let defaults = UserDefaults.standard
                    let billing:NSDictionary = defaults.object(forKey: "billingaddressDICT")as! NSDictionary
        
                    print("billing dict - \(billing)")
        
       
        
        let addline1Lblstr = billing["name"]as! String
        
        let one = billing["address"]as! String
        let two = billing["house_number"]as! String
        let addline2Lblstr = one + " " + two
        let addline3Lblstr = billing["city"]as! String
        let addline5Lbl = billing["country"]as! String
        let addline6Lbl = billing["zip"] as! String
        
        addline1Lbl.text = addline1Lblstr
        addline2Lbl.text = addline2Lblstr + "," + addline3Lblstr
        addline3Lbl.text = addline5Lbl + "," + addline6Lbl
    }
   

    @IBAction func menuBtnClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
    }
   
    @IBAction func homeBtnClicked(_ sender: UIButton) {
    
    let home = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
    self.navigationController?.pushViewController(home, animated: true)
        
    }
    
    
}

extension SummaryPage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
    }
}

// MARK: - UICollectionViewDataSource
extension SummaryPage: UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return fetchedItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! summaryCell
        
    cell1.cellimageView.layer.masksToBounds = false
    cell1.cellimageView.layer.cornerRadius = cell1.cellimageView.frame.size.width / 2
    cell1.cellimageView.clipsToBounds = true
    
    cell1.cellouterView.layer.cornerRadius = 8
    cell1.cellouterView.layer.shadowColor = UIColor.lightGray.cgColor
    cell1.cellouterView.layer.shadowOpacity = 1
    cell1.cellouterView.layer.shadowOffset = .zero

    if fetchedItems.count == 0 {
        
    }else{
    
    let dictObj = self.fetchedItems[indexPath.row] as! NSDictionary

    cell1.cellProName.text = dictObj["product_name"] as? String

        let rupee = "$"

        let pricedata = dictObj["line_total"]as! String

       cell1.cellpriceLbl.text = rupee + pricedata
        
        var urlStr = String()

        if dictObj["product_url"] is NSNull {
            urlStr = ""
        }else{

            urlStr = dictObj["product_url"] as! String

        }

            let qtyLbl = "Qty : "
            
            let qnt = dictObj["quantity"] as! Int
            cell1.cellQtyLbl.text = qtyLbl + String(qnt)

        let url = URL(string: urlStr )

       cell1.cellimageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell1.cellimageView.sd_setImage(with: url) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell1.cellimageView.image = UIImage(named: "noimage.png")
            } else {
                // Successful in loading image
                cell1.cellimageView.image = image
            }
        }

    }
    
        return cell1
    
}
    
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {


            let cellSize = CGSize(width: 130, height: 200)
            return cellSize

       

    }
    
}

// MARK: - Api List
extension SummaryPage{
    
func orderitems()  {
    
    let defaults = UserDefaults.standard
    
  //  let admintoken = defaults.object(forKey: "adminToken")as? String
    
    let admintoken = defaults.object(forKey: "custToken")as? String
       
    
    let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
  //  let trimmedString = categoryStr.removingAllWhitespaces()
    
    let urlString = GlobalClass.DevlopmentApi+"order-item/?order_id=\(orderIDGet)"
    
   

        
    print("oderitems get url - \(urlString)")
    
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": autho
        ]

    AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                            
                                let dict1 :NSDictionary = response.value! as! NSDictionary
                                 
                               self.fetchedItems = (dict1.value(forKey:"results")as! NSArray)
                                
                                   print(self.fetchedItems)
                                
                                     if self.fetchedItems.count == 0 {
                                       
                                        self.sellerCollectionView.reloadData()
                                        
                                        ERProgressHud.sharedInstance.hide()
                                     }else{
                                   
//                                        tableheight.constant = producttable.estimatedRowHeight + 100 * CGFloat(fetchedItems.count)

                                        self.sellerCollectionView.reloadData()
                                        
                                        ERProgressHud.sharedInstance.hide()

                                     }
                
                             
                            }else{
                                
                    if response.response?.statusCode == 401{
                                
                        ERProgressHud.sharedInstance.hide()
                    self.SessionAlert()
                      
                       }else if response.response?.statusCode == 500{
                                    
                        ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                       }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                
                                
                                
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }
        
  
        
    }


func loadBillingAddress(){
    
    let defaults = UserDefaults.standard
    
 //   let admintoken = defaults.object(forKey: "adminToken")as? String
    let admintoken = defaults.object(forKey: "custToken")as? String
    let customerId = defaults.integer(forKey: "custId")
    
    let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
    
    let urlString = GlobalClass.DevlopmentApi + "billing/?customer_id=\(customerId)"
    
    print("category Url -\(urlString)")
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": autho
        ]

    AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
        response in
          switch response.result {
                        case .success:
                           // print(response)

                            if response.response?.statusCode == 200{
                             
                              
                                
                             let dict1 :NSDictionary = response.value! as! NSDictionary
                                
                                print("billing address - \(dict1)")
                              
                                let billingaddressData = (dict1.value(forKey:"results")as! NSArray)
                                
                                if billingaddressData.count == 0 {
                             
                           }else{

                 
                            let firstobj:NSDictionary  = billingaddressData.object(at: 0) as! NSDictionary
                           
                            let addline1Lblstr = firstobj["name"]as! String
                            
                            let one = firstobj["address"]as! String
                            let two = firstobj["house_number"]as! String
                            let addline2Lblstr = one + " " + two
                            let addline3Lblstr = firstobj["city"]as! String
                            let addline4Lbl = firstobj["state"]as! String
                            let addline5Lbl = firstobj["country"]as! String
                            let addline6Lbl = firstobj["zip"] as! String
                             
                            addline1Lbl.text = addline1Lblstr
                            addline2Lbl.text = addline2Lblstr + "," + addline3Lblstr
                            addline3Lbl.text = addline4Lbl + "," + addline5Lbl + "," + addline6Lbl
                           }
                                
                              
                             
                            }else{
                                
                                if response.response?.statusCode == 401{
                                
                                    ERProgressHud.sharedInstance.hide()

                                    self.SessionAlert()
                                    
                                }else if response.response?.statusCode == 404{
                                    
                                   
                                    
                                    
                                }else if response.response?.statusCode == 500{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    
                                }
                                
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()
                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }
        
  
        
    }
}

// MARK: - AlertController
extension SummaryPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
