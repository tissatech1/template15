//
//  ChangePasswordPage.swift
//  Template4
//
//  Created by TISSA Technology on 3/12/21.
//

import UIKit
import SideMenu
import Alamofire

class ChangePasswordPage: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    @IBOutlet weak var oldpassTf: UITextField!
    @IBOutlet weak var newpassTf: UITextField!
    @IBOutlet weak var confirmPasstf: UITextField!
    
    var usernameget = String()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateButton.layer.cornerRadius = 20
        
        view1.layer.cornerRadius = 8
        view1.layer.shadowColor = UIColor.lightGray.cgColor
        view1.layer.shadowOpacity = 1
        view1.layer.shadowOffset = .zero
        view1.layer.shadowRadius = 8
        
        view2.layer.cornerRadius = 8
        view2.layer.shadowColor = UIColor.lightGray.cgColor
        view2.layer.shadowOpacity = 1
        view2.layer.shadowOffset = .zero
        view2.layer.shadowRadius = 8
        
        view3.layer.cornerRadius = 8
        view3.layer.shadowColor = UIColor.lightGray.cgColor
        view3.layer.shadowOpacity = 1
        view3.layer.shadowOffset = .zero
        view3.layer.shadowRadius = 8
    }
    

    func randomColor() -> UIColor{
            let red = CGFloat(drand48())
            let green = CGFloat(drand48())
            let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 0.5)
    }
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
    }
   
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }

    @IBAction func changepasssubmit(_ sender: UIButton) {
     
        if oldpassTf.text == "Current password" || oldpassTf.text == ""{
            showSimpleAlert(messagess: "Enter current password")
        }else if newpassTf.text == "New password" || newpassTf.text == ""{
            
            showSimpleAlert(messagess: "Enter new password")
        }else if newpassTf.text!.count <= 8{
            
            showSimpleAlert(messagess: "Password must be greater than or equal to 8 characters")
        }else if confirmPasstf.text == "Confirm password" || confirmPasstf.text == ""{
            
            showSimpleAlert(messagess: "Enter confirm password")
        }else if confirmPasstf.text! != newpassTf.text!{
            
            showSimpleAlert(messagess: "New password and confirm password does not match")
        }else{
            
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
            changepasswordApi()
            
        }
   
}

}

extension ChangePasswordPage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
    }
}

// MARK: - Api
extension ChangePasswordPage {
    
    func changepasswordApi() {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
       // let customerId = defaults.integer(forKey: "custId")
        
      
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "/rest-auth/password/reset/confirm/v1/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .post, parameters: ["username":usernameget, "old_password":oldpassTf.text!,"new_password1":newpassTf.text!,"new_password2":confirmPasstf.text!],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    ERProgressHud.sharedInstance.hide()
                                    let alert = UIAlertController(title:nil , message: "Password changed successfully",         preferredStyle: UIAlertController.Style.alert)

                                  
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                
                                                                    
                                                                    UserDefaults.standard.removeObject(forKey: "custToken")
                                                                    UserDefaults.standard.removeObject(forKey: "custId")
                                                                UserDefaults.standard.removeObject(forKey: "Usertype")
                                                                    
                                                                    let chpass = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                    
                                                                    self.navigationController?.pushViewController(chpass, animated: true)

                                                                    
                                                                    
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                    
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
}

// MARK: - AlertController
extension ChangePasswordPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
