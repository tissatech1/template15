//
//  AboutUsPage.swift
//  Template4
//
//  Created by TISSA Technology on 3/10/21.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage
class AboutUsPage: UIViewController {
    @IBOutlet weak var imageviewShow: UIImageView!
    @IBOutlet weak var textviewShow: UITextView!
    @IBOutlet weak var textbackView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        textviewShow.layer.cornerRadius = 10
        textbackView.backgroundColor = randomColor()
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        getrestaurantApi()
    }
    
    func randomColor() -> UIColor{
            let red = CGFloat(drand48())
            let green = CGFloat(drand48())
            let blue = CGFloat(drand48())
        return UIColor(red: red, green: green, blue: blue, alpha: 0.5)
    }
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
    }
   
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }

}

extension AboutUsPage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
    }
}

// MARK: - Api
extension AboutUsPage {

    func getrestaurantApi(){
        
        var admintoken = String()
        let defaults = UserDefaults.standard
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
       
            admintoken = (defaults.object(forKey: "adminToken")as? String)!
            
        }else{
            
            admintoken = (defaults.object(forKey: "custToken")as? String)!
            
        }
        
        let autho = "token \(admintoken)"
        
        let urlString = GlobalClass.DevlopmentApi+"restaurant/\(GlobalClass.restaurantGlobalid)/"
        
           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]
      

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                   
                                   let data  = dict["desc"]as? String
                                    
                                    print(data as Any)
//                                    self.abouTxtView.text! = data ?? "About Us"
                                    let strtag = data ?? "About Us"
                                    
                                //    let str = strtag.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)

                                    self.textviewShow.text! = strtag
                                  
                                    var urlStr = String()
                                    if dict["restaurant_url"] is NSNull || dict["restaurant_url"] == nil{

                                        urlStr = ""

                                    }else{
                                        urlStr = dict["restaurant_url"] as! String
                                    }

                                    
                                    let url = URL(string: urlStr )

                                    self.imageviewShow.sd_imageIndicator = SDWebImageActivityIndicator.gray
                                    self.imageviewShow.sd_setImage(with: url) { (image, error, cache, urls) in
                                        if (error != nil) {
                                            // Failed to load image
                                            self.imageviewShow.image = UIImage(named: "aboutnoimage.png")
                                        } else {
                                            // Successful in loading image
                                            self.imageviewShow.image = image
                                        }
                                    }
                                    
                                    
                                    
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()

                    self.SessionAlert()
                          
                                    
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
}


// MARK: - AlertController
extension AboutUsPage {
  
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
}
